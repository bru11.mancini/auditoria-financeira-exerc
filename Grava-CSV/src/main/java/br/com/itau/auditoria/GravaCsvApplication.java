package br.com.itau.auditoria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GravaCsvApplication {

	public static void main(String[] args) {
		SpringApplication.run(GravaCsvApplication.class, args);
	}

}
