package br.com.itau.auditoria.consumer;


import br.com.itau.auditoria.csv.GravaCsv;
import br.com.itau.auditoria.producer.EmpresaCompleta;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class EmpresaCompletaConsumer {



    EmpresaCompleta empresaCompleta;

    @KafkaListener(topics = "spec3-bruno-mancini-3", groupId = "consulta-cnpj-completo")
    public void receberCnpjCompleto(@Payload EmpresaCompleta empresaCompleta) throws CsvRequiredFieldEmptyException, IOException, CsvDataTypeMismatchException {
        System.out.println("Recebi a empresa completa " + empresaCompleta.getFantasia() + "às" + LocalDateTime.now());
        GravaCsv gravaCsv = new GravaCsv();
        gravaCsv.gravarArquivo(empresaCompleta);


    }
}
