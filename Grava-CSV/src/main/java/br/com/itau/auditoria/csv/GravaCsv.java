package br.com.itau.auditoria.csv;

import br.com.itau.auditoria.producer.EmpresaCompleta;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class GravaCsv {

    @Autowired
    EmpresaCompleta empresaCompleta;

    public void gravarArquivo(EmpresaCompleta empresaCompleta) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        List<EmpresaCompleta> empresaCompletaArrayList = new ArrayList<>();
        empresaCompletaArrayList.add(empresaCompleta);

        Writer writer = Files.newBufferedWriter(Paths.get("/home/a2w/IdeaProjects/Auditoria Financeira/arquivo.csv"));
        StatefulBeanToCsv<EmpresaCompleta> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(empresaCompletaArrayList);
        writer.flush();
        writer.close();
        System.out.println("Carregou arquivo");

    }
}
