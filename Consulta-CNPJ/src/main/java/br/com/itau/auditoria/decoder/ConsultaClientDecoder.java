package br.com.itau.auditoria.decoder;

import br.com.itau.auditoria.exceptions.ConsultaNotFoudException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ConsultaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response){
        if (response.status() == 400){
            return new ConsultaNotFoudException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}