package br.com.itau.auditoria.client;

import br.com.itau.auditoria.decoder.ConsultaClientDecoder;
import br.com.itau.auditoria.fallback.ConsultaClientFallback;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ConsultaClientConfiguration {

    @Bean
    public ErrorDecoder getErroDecoder(){

        return new ConsultaClientDecoder();
    }

//    @Bean
//    public Feign.Builder builder(){
//        FeignDecorators feignDecorators = FeignDecorators.builder()
//                .withFallback(new ConsultaClientFallback(), RetryableException.class)
//                .build();
//        return Resilience4jFeign.builder(feignDecorators);
//    }
}
