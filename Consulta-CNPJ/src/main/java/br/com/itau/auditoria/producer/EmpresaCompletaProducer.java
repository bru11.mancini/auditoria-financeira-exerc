package br.com.itau.auditoria.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class EmpresaCompletaProducer {
    @Autowired
    private KafkaTemplate<String, EmpresaCompleta> producer;

    public void enviarAoKafka(EmpresaCompleta empresaCompleta) {
        producer.send("spec3-bruno-mancini-3", empresaCompleta);
    }
}
