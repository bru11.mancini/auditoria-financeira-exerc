package br.com.itau.auditoria.service;

import br.com.itau.auditoria.client.ConsultaClient;
import br.com.itau.auditoria.producer.Empresa;
import br.com.itau.auditoria.producer.EmpresaCompleta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class DadosCnpj {

    @Autowired
    ConsultaClient consultaClient;

    public EmpresaCompleta ConsultaDados(String cnpj){


        EmpresaCompleta empresaCompleta = consultaClient.buscarDadosReceitaPorCnpj(cnpj);
        System.out.println("Passou aqui");

        return empresaCompleta;

    }
}
