package br.com.itau.auditoria.consumer;

import br.com.itau.auditoria.producer.EmpresaCompleta;
import br.com.itau.auditoria.producer.Empresa;
import br.com.itau.auditoria.service.DadosCnpj;
import br.com.itau.auditoria.service.EmpresaCompletaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CnpjConsumer {

    @Autowired
    EmpresaCompletaService empresaCompletaService;

    @Autowired
    DadosCnpj dadosCnpj;

    EmpresaCompleta empresaCompleta;

    @KafkaListener(topics = "spec3-bruno-mancini-2", groupId = "consulta-cnpj")
    public void receberCnpj(@Payload Empresa empresa){
        System.out.println("Recebi a empresa " + empresa.getCnpj() + "às" + LocalDateTime.now());

        EmpresaCompleta empresaCompleta = dadosCnpj.ConsultaDados(empresa.getCnpj());

        empresaCompletaService.repassarEmpresaCompleta(empresaCompleta);

    }
}
