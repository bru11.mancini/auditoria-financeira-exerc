package br.com.itau.auditoria.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O sistema de consulta se encontra offline")
public class ConsultaOfflineException extends RuntimeException{
}
