package br.com.itau.auditoria.service;

import br.com.itau.auditoria.producer.EmpresaCompleta;
import br.com.itau.auditoria.producer.EmpresaCompletaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class EmpresaCompletaService {

    @Autowired
    EmpresaCompletaProducer empresaCompletaProducer;

    EmpresaCompleta empresaCompleta;

    public EmpresaCompleta repassarEmpresaCompleta(EmpresaCompleta empresaCompleta){
        if (Double.parseDouble(empresaCompleta.getCapital_social()) > 1000000.00){
            empresaCompletaProducer.enviarAoKafka(empresaCompleta);
            System.out.println("Enviei a empresa completa às " + LocalDateTime.now());
        }

        return empresaCompleta;
    }
}
