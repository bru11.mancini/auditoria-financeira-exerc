package br.com.itau.auditoria.client;

import br.com.itau.auditoria.producer.EmpresaCompleta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "consultacnpj", url = "https://www.receitaws.com.br", configuration = ConsultaClientConfiguration.class)
public interface ConsultaClient {

    @GetMapping("/v1/cnpj/{cnpj}")
    EmpresaCompleta buscarDadosReceitaPorCnpj(@PathVariable String cnpj);
}
