package br.com.itau.auditoria.producer;

public class EmpresaCompleta {
    private String status;
    private String message;
    private String tipo;
    private String abertura;
    private String nome;
    private String fantasia;
    private String capital_social;

    public EmpresaCompleta() {
    }

    public EmpresaCompleta(String status, String message, String tipo, String abertura, String nome, String fantasia, String capital_social) {
        this.status = status;
        this.message = message;
        this.tipo = tipo;
        this.abertura = abertura;
        this.nome = nome;
        this.fantasia = fantasia;
        this.capital_social = capital_social;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getAbertura() {
        return abertura;
    }

    public void setAbertura(String abertura) {
        this.abertura = abertura;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public String getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(String capital_social) {
        this.capital_social = capital_social;
    }
}
