package br.com.itau.auditoria.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Consulta não encontrado. Mensagem vinda da ConsultaNotFoundException")
public class ConsultaNotFoudException extends RuntimeException {
}
