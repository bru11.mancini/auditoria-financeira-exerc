package br.com.itau.auditoria.fallback;

import br.com.itau.auditoria.client.ConsultaClient;
import br.com.itau.auditoria.exceptions.ConsultaOfflineException;
import br.com.itau.auditoria.producer.EmpresaCompleta;

public class ConsultaClientFallback implements ConsultaClient {

    @Override
    public EmpresaCompleta buscarDadosReceitaPorCnpj(String cnpj){
        throw new ConsultaOfflineException();
    }
}
