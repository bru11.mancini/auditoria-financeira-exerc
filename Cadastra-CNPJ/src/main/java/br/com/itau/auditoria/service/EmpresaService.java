package br.com.itau.auditoria.service;

import br.com.itau.auditoria.producer.Empresa;
import br.com.itau.auditoria.producer.EmpresaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class EmpresaService {

    @Autowired
    EmpresaProducer empresaProducer;

    Empresa empresa;

    public Empresa cadastrarEmpresa(Empresa empresa){
        System.out.println("Enviei a mensagem às " + LocalDateTime.now());
        empresaProducer.enviarAoKafka(empresa);
        return empresa;
    }
}
