package br.com.itau.auditoria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CadastraCnpjApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastraCnpjApplication.class, args);
	}

}
