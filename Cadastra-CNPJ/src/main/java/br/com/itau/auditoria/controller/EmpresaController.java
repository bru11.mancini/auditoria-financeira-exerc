package br.com.itau.auditoria.controller;

import br.com.itau.auditoria.producer.Empresa;
import br.com.itau.auditoria.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmpresaController {

    @Autowired
    EmpresaService empresaService;

    private Empresa empresa;

    @PostMapping("/empresa")
    @ResponseStatus(HttpStatus.CREATED)
    public Empresa cadastrarEmpresa(@RequestBody Empresa empresa){
        return empresaService.cadastrarEmpresa(empresa);

    }

}
